package test.selenium.myproject;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.*;
import org.openqa.selenium.WebElement;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	WebDriver driver;
    	System.setProperty("webdriver.firefox.marionette","/usr/local/bin/geckodriver");
		driver = new FirefoxDriver();
		driver.get("http://52.66.187.194:8181/testServlet-0.0.1-SNAPSHOT");
		
		driver.findElement(By.name("no1")).sendKeys("12");
		driver.findElement(By.name("no2")).sendKeys("14");
		List<WebElement> radiobuttons = driver.findElements(By.name("r1"));
		for(WebElement radiobutton: radiobuttons)
		{
		if(radiobutton.getAttribute("value").equals("Mul"))
		        radiobutton.click();
		}
		driver.findElement(By.name("submit")).click();
		String result = driver.findElement(By.xpath("//h1")).getAttribute("innerHTML");
		if(result.contains("168"))
		        System.out.println("Test Case passed");
		else
		        System.out.println("Test Case Failed");
		

    }
}



/*
 import java.util.*;
import org.openqa.selenium.WebElement;

driver.findElement(By.name("no1")).sendKeys("12");
driver.findElement(By.name("no2")).sendKeys("14");
List<WebElement> radiobuttons = driver.findElements(By.name("r1"));
for(WebElement radiobutton: radiobuttons)
{
if(radiobutton.getAttribute("value").equals("Mul"))
        radiobutton.click();
}
driver.findElement(By.name("submit")).click();
String result = driver.findElement(By.xpath("//h1")).getAttribute("innerHTML");
if(result.contains("168"))
        System.out.println("Test Case passed");
else
        System.out.println("Test Case Failed");

*/